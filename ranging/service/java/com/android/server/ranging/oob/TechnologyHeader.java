/*
 * Copyright 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.ranging.oob;

import com.android.server.ranging.RangingTechnology;

import com.google.auto.value.AutoValue;

/** Header for individual technology capability. */
@AutoValue
public abstract class TechnologyHeader {

    public static final int SIZE_BYTES = 2;

    public static TechnologyHeader parseBytes(byte[] payload) {
        if (payload.length < SIZE_BYTES) {
            throw new IllegalArgumentException(
                    String.format(
                            "Header is too short, expected at least %d bytes, got %d",
                            SIZE_BYTES, payload.length));
        }

        int parseCursor = 0;
        byte technologyId = payload[parseCursor++];
        RangingTechnology rangingTechnology;
        try {
            rangingTechnology = RangingTechnology.TECHNOLOGIES.get(technologyId);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(
                    "Attempted to parse unknown technology with id" + technologyId);
        }
        int size = payload[parseCursor++];

        return builder().setRangingTechnology(rangingTechnology).setSize(size).build();
    }

    public byte[] toBytes() {
        byte[] payload = new byte[SIZE_BYTES];
        int parseCursor = 0;
        payload[parseCursor++] = getRangingTechnology().toByte();
        payload[parseCursor++] = (byte) getSize();
        return payload;
    }

    public int getHeaderSize() {
        return SIZE_BYTES;
    }

    /** Returns the version. */
    public abstract RangingTechnology getRangingTechnology();

    /** Returns the message type. */
    public abstract int getSize();

    /** Returns a builder for {@link TechnologyHeader}. */
    public static Builder builder() {
        return new AutoValue_TechnologyHeader.Builder();
    }

    /** Builder for {@link TechnologyHeader}. */
    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setSize(int size);

        public abstract Builder setRangingTechnology(RangingTechnology rangingTechnology);

        public abstract TechnologyHeader build();
    }
}
